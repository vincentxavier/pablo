#!/bin/bash

which vim >/dev/null && export EDITOR="vim"
export TEXINPUTS="$TEXINPUTS:$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
